export default function Card({ title, name, data, dataValue, value }) {
  return (
    <div className="w-full h-full">
      <h3 className="text-white mb-2 w-full  text-center">{title}</h3>
      <div className="w-full h-full flex justify-between text-black rounded p-4 bg-white">
        <div className="w-full h-full mt-8">
          <h2 className="text-2xl font-medium mb-2">{name}</h2>
          <div className="flex flex-col">
            <div className="flex flex-row">
              <p>{data?.data1}: </p>
              <p className="ml-2">{dataValue?.value1 || 'Nenhum dado informado'}</p>
            </div>
            <div className="flex flex-row">
              <p>{data?.data2}: </p>
              <p className="ml-2">{dataValue?.value2 || 'Nenhum dado informado'}</p>
            </div>
          </div>
        </div>
        <div className="w-full h-full flex flex-col items-end">
          <h1 className="font-bold text-8xl">{value?.value}</h1>
          <p className="text-base font-medium">{value?.name}</p>
        </div>
      </div>
    </div>
  );
}
