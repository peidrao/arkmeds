import { useState, useEffect } from "react";
import "./index.css";
import ArkmedsLogo from "./asset/Arkmeds.svg";
import Card from "./components/Card.js";
import { AiFillFlag } from "react-icons/ai";

import api from "./services/apis";

function App() {
  const [equipament, setEquipament] = useState({});
  const [company, setCompany] = useState({});
  const [calls, SetCalls] = useState([]);

  useEffect(() => {
    api
      .get("api/equipament/more_calls/")
      .then((response) => setEquipament(response.data))
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    api
      .get("api/company/more_equipaments/")
      .then((response) => setCompany(response.data))
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    api
      .get("api/equipament/calls/")
      .then((response) => SetCalls(response.data))
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className="w-full px-10">
      <div className="w-full flex justify-center mt-20">
        <img src={ArkmedsLogo} alt="Arkmeds Logo" />
      </div>

      <div className="grid grid-cols-2 gap-10 mt-12">
        <Card
          title="Empresa com mais equipamentos"
          name={company.name}
          data={{
            data1: "CNPJ",
            data2: "E-mail",
          }}
          dataValue={{
            value1: company.cnpj,
            value2: company.email,
          }}
          value={{ value: company.equipaments, name: "equipamentos" }}
        />
        <Card
          title="Equipamentos com mais chamadas"
          name={`ID: ${equipament.id || "Nenhum dado informado"}`}
          data={{
            data1: "Modelo",
            data2: "Nº Serial",
          }}
          dataValue={{
            value1: equipament.model,
            value2: equipament.serial_number,
          }}
          value={{ value: equipament.calls, name: "chamadas" }}
        />
      </div>
      <div
        className="w-full bg-white rounded p-5 mt-20 overflow-auto"
        style={{ height: "500px" }}
      >
        <div className="mb-6">
          <h2 className="font-medium text-lg">Chamados</h2>
          <h3 className="font-semibold text-xs text-gray-600">
            Quantidade de chamados: {calls.length}
          </h3>
        </div>

        <div className="flex flex-col w-full items-start">
          <table className="w-full">
            <thead className="sticky -top-5 w-full">
              <tr className="bg-primary font-semibold text-white w-full">
                <th className="p-5 w-1/2 flex items-start">ID</th>
                <th className="p-5 ">Número</th>
                <th className="p-5 w-2/6 flex">Proprietário</th>
                <th className="p-5 w-2/6">Equipamento</th>
                <th className="p-5 w-1/6 ">Prioridade</th>
              </tr>
            </thead>
            <tbody>
              {calls.map((item, index) => {
                return (
                  <tr key={`calls-${index}`}>
                    <td className="px-5 p-2">{item.id}</td>
                    <td className="flex justify-center p-2">{item.number}</td>
                    <td className="w-2/6 pl-5">
                      {item.nickname_owner || "Valor não informado"}
                    </td>
                    <td className="w-2/6 flex justify-end">
                      {item.equipament_name}
                    </td>
                    <td className="w-1/6 " style={{ color: item.color_code }}>
                      <AiFillFlag />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
