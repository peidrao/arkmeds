from django.db.models import Count
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .serializers import CallSerializer, EquipamentSerializer
from equipment.models import Call, Equipament


@api_view(['GET'])
def get_calls(request):
    calls = Call.objects.all()
    serializer = CallSerializer(calls, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def equipament_more_calls(request):
    equipament_more  = Call.objects.all().values('equipament').annotate(calls=Count('equipament')).order_by('-calls').first()
    company = Equipament.objects.get(id=equipament_more['equipament'])
    equipament_more.pop('equipament')
    serializer = EquipamentSerializer(company, many=False)
    
    equipament_more.update(serializer.data)
    return Response(equipament_more)