from django.urls import path
from . import views

urlpatterns = [
    path('calls/', views.get_calls, name='calls'),
    path('more_calls/', views.equipament_more_calls,
         name='more_calls'),
]
