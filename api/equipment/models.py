from django.db import models

from company.models import Company

# Create your models here.
class Equipament(models.Model):
    id = models.IntegerField(primary_key=True)
    owner = models.ForeignKey(Company, on_delete=models.DO_NOTHING, blank=True, null=True)
    manufacturer = models.CharField(max_length=100, blank=True, null=True)
    model  = models.CharField(max_length=100, blank=True, null=True)
    patrimony  = models.CharField(max_length=100, blank=True, null=True)
    serial_number = models.CharField(max_length=100, blank=True, null=True)


class Call(models.Model):
    id = models.IntegerField(primary_key=True)
    color_code = models.CharField(max_length=7, null=True, blank=True)
    number = models.CharField(max_length=10, null=True, blank=True)
    nickname_owner = models.CharField(max_length=30, null=True, blank=True)
    equipament_name = models.CharField(max_length=50, null=True, blank=True)
    equipament = models.ForeignKey(Equipament, on_delete=models.DO_NOTHING, null=True)
