from rest_framework import serializers

from .models import Call, Equipament


class CallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Call
        fields = "__all__"


class EquipamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipament
        fields = "__all__"