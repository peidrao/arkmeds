# Generated by Django 3.2.9 on 2021-11-12 20:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0006_auto_20211112_1908'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipament',
            name='equipament_id',
        ),
        migrations.AlterField(
            model_name='equipament',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
