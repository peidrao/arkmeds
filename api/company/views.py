from rest_framework.response import Response
from django.db.models import Count
from rest_framework.decorators import api_view

from .models import Company
from .serializers import CompanySerializer
from equipment.models import Equipament


@api_view(['GET'])
def get_companies(request):
    companies = Company.objects.all()
    serializer = CompanySerializer(companies, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def company_more_equipaments(request):
    company_more = Equipament.objects.all().values('owner').annotate(
        equipaments=Count('owner')).order_by('-equipaments').first()
    company = Company.objects.get(id=company_more['owner'])
    company_more.pop('owner')
    serializer = CompanySerializer(company, many=False)

    company_more.update(serializer.data)
    return Response(company_more)
