import datetime
import json
import os
import django
import requests
import random
import string

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api.settings')
django.setup()

from equipment.models import Equipament, Call
from company.models import Company


def get_token():
    headers = {
        'Content-Type': 'application/json'
    }

    payload = json.dumps({
        "email": "testedev@arkmeds.com",
        "password": "testedev"
    })

    url = 'https://desenvolvimento.arkmeds.com/rest-auth/token-auth/'

    response = requests.post(url, data=payload, headers=headers)

    return response.json()['token']


def get_companies(headers):
    url = "https://desenvolvimento.arkmeds.com/api/v2/company/"
    response = requests.get(url, headers=headers)

    for company in response.json()['results']:
        if company['tipo'] != 5:
            Company(
                id=company['id'],
                cnpj=company['cnpj'],
                type=company['tipo'],
                name=company['nome'],
                fantasy_name=company['nome_fantasia'],
                comments=company['observacoes'],
                contact=company['contato'],
                email=company['email'],
                phone=company['telefone1'],
                phone2=company['telefone2'],
                extension=company['ramal1'],
                extension2=company['ramal2'],
                fax=company['fax'],
                zip_code=company['cep'],
                number=company['numero'],
                neighborhood=company['bairro'],
                city=company['cidade'],
                state=company['estado']
            ).save()


def get_equipaments(headers):
    for company in Company.objects.all().order_by('-id'):
        url = f'https://desenvolvimento.arkmeds.com/api/v2/equipamentos_paginados/?empresa_id={company.id}'
        response = requests.get(url, headers=headers)
        if response.json()['results']:
            for equipament in response.json()['results']:
                Equipament(
                    id=equipament['id'],
                    owner_id=company.id,
                    manufacturer=equipament['fabricante'],
                    model=equipament['modelo'],
                    patrimony=equipament['patrimonio'],
                    serial_number=equipament['numero_serie']).save()


def get_random_string():
    letters = string.ascii_lowercase
    result = ''.join(random.choice(letters) for i in range(99))
    return result


def create_called(headers):
    worlds = get_random_string()
    url = 'https://desenvolvimento.arkmeds.com/api/v1/chamado/novo/'
    for equipament in Equipament.objects.all():
        payload = json.dumps({
            "equipamento": equipament.id,
            "solicitante": equipament.owner.id,
            "tipo_servico": 3,
            "problema": 5,
            "observacoes": worlds,
            "data_criacao": int(datetime.datetime.utcnow().timestamp()),
            "id_tipo_ordem_servico": 1
        })

        response = requests.post(url, headers=headers, data=payload)
        print(response.json())


def get_calls(headers):
    for equipament in Equipament.objects.values_list('id', 'owner__fantasy_name').distinct():
        url = f'https://desenvolvimento.arkmeds.com/api/v2/chamado/?equipamento_id={equipament[0]}'
        response = requests.get(url, headers=headers)

        if response.json()['results']:
            for call in response.json()['results']:
                Call(
                    id=call['chamados'],
                    color_code=call['cor_prioridade'],
                    number=call['numero'],
                    equipament_name=call['get_equipamento_servico'],
                    nickname_owner=equipament[1],
                    equipament_id=equipament[0]
                ).save()


def main():
    token = get_token()
    headers = {
        'Authorization': f'JWT {token}', 'Content-Type': 'application/json'}
    get_companies(headers)
    get_equipaments(headers)
    create_called(headers)
    get_calls(headers)


if __name__ == '__main__':
    main()
