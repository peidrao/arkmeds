# Desafio Arkmeds


Todas as três partes do desafio foram feitas


## 1 - Instalação

1. Clone o repositório

### 1.1 - API

2. Crie um ambiente virtual para a instalação das depedências do 
    - **Linux**   
        - `python -m virtualenv [nome_do_ambiente_virtual]`
        - `source [nome_do_ambiente_virtual]/bin/activate`
    - [**Windows**](https://medium.com/co-learning-lounge/create-virtual-environment-python-windows-2021-d947c3a3ca78)
        - `python3 -m venv [nome_do_ambiente_virtual]`
        - `python3 -m venv [nome_do_ambiente_virtual]`

3. Após a criação do ambiente virtual, precisamos instalar as depêndencias do projeto que estão no arquivo _requirements.txt_

    - `` pip install -r requirements.txt``

4. Tendo as depêndecias do projeto instaladas. Podemos rodar a API do projeto

    - ``python manage.py makemigratios`` (para criar os arquivos de migrações)
    - ``python manage.py migrate`` (criar as migrações no banco de dados)
    -``python manage.py runserver`` (rodar o projeto)


### 1.2 - React

5. No diretório web-arkmeds
    - `yarn install` e logo depois 

    - `yarn start` 


## Docker 
O projeto por padrão está usando o sqlite, entretanto, caso queira ser usado o banco de dados Postgres, basta em **api/api/settings.py**, descomentar o banco relacionado ao postgres e comentar o sqlite. Ao fazer isso repita o passo 4 para que as alterações possam ter efeito no novo banco de dados. É importante que antes de tudo isso, você tenha dado o comando 

 - `docker-compose up -d`